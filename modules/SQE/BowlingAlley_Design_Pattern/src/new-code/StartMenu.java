import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.*;
import java.text.*;

public class StartMenu implements ActionListener {

	private JFrame win;
	private JButton abort, finished;
	private JLabel lanesLabel, partysizeLabel, errorLabel, difficultyLabel;
	private JTextField lanesField, partysizeField;
	private String lanes, partysize;
	private DefaultComboBoxModel difficultyNames;
	private JComboBox difficultyCombo;
	private JScrollPane difficultyScrollPane;
	private boolean error, done;
	private String errorMessage;

	public StartMenu() {
		done = false;
		error = false;
		errorMessage = "";
		lanes = "3";
		partysize = "5";
		win = new JFrame("Start Menu");
		addPanels();
		// Center Window on Screen
		Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
		win.setLocation(
			((screenSize.width) / 2) - ((win.getSize().width) / 2),
			((screenSize.height) / 2) - ((win.getSize().height) / 2));
		win.show();
	}

	private void addPanels() {
		win.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		win.getContentPane().setLayout(new BorderLayout());
		((JPanel) win.getContentPane()).setOpaque(false);

		JPanel colPanel = new JPanel();
		colPanel.setLayout(new BorderLayout());

		// Settings Panel
		JPanel settingsPanel = new JPanel();
		if(error) settingsPanel.setLayout(new GridLayout(4, 1));
		else settingsPanel.setLayout(new GridLayout(3, 1));
		settingsPanel.setBorder(new TitledBorder("Settings"));

		JPanel lanesPanel = new JPanel();
		lanesPanel.setLayout(new FlowLayout());
		lanesLabel = new JLabel("Number of lanes:");
		lanesField = new JTextField(lanes, 2);
		lanesPanel.add(lanesLabel);
		lanesPanel.add(lanesField);

		JPanel partysizePanel = new JPanel();
		partysizePanel.setLayout(new FlowLayout());
		partysizeLabel = new JLabel("Max patrons per party:");
		partysizeField = new JTextField(partysize, 2);
		partysizePanel.add(partysizeLabel);
		partysizePanel.add(partysizeField);

		JPanel errorPanel = new JPanel();
		errorPanel.setLayout(new FlowLayout());
		errorLabel = new JLabel(errorMessage);
		errorLabel.setFont(new Font("Arial", Font.PLAIN, 10));
		errorLabel.setForeground(Color.red);
		errorPanel.add(errorLabel);

		JPanel difficultyPanel = new JPanel();
		difficultyPanel.setLayout(new FlowLayout());
		difficultyLabel = new JLabel("Difficulty:");
		difficultyNames = new DefaultComboBoxModel();
		difficultyNames.addElement("Easy");
		difficultyNames.addElement("Medium");
		difficultyNames.addElement("Hard");
		difficultyCombo = new JComboBox(difficultyNames);
		difficultyCombo.setSelectedIndex(1);
	    difficultyScrollPane = new JScrollPane(difficultyCombo);
		difficultyPanel.add(difficultyLabel);
		difficultyPanel.add(difficultyScrollPane);

		settingsPanel.add(lanesPanel);
		settingsPanel.add(partysizePanel);
		if(error) settingsPanel.add(errorPanel);
		settingsPanel.add(difficultyPanel);

		// Button Panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 2));

		Insets buttonMargin = new Insets(4, 4, 4, 4);

		finished = new JButton("Start!");
		JPanel finishedPanel = new JPanel();
		finishedPanel.setLayout(new FlowLayout());
		finished.addActionListener(this);
		finishedPanel.add(finished);

		abort = new JButton("Quit");
		JPanel abortPanel = new JPanel();
		abortPanel.setLayout(new FlowLayout());
		abort.addActionListener(this);
		abortPanel.add(abort);

		buttonPanel.add(abortPanel);
		buttonPanel.add(finishedPanel);

		// Clean up main panel
		colPanel.add(settingsPanel, "Center");
		colPanel.add(buttonPanel, "South");

		win.getContentPane().add("Center", colPanel);

		win.pack();
	}

	private boolean check() {
		int num_lanes, max_ppp;
		try {
			num_lanes = Integer.parseInt(lanes);
			max_ppp = Integer.parseInt(partysize);
		}
		catch (Exception e) {
			errorMessage = "Invalid input: Both the text fields should contain integers.";
			return false;
		}
		if(num_lanes < 1) {
			errorMessage = "Invalid input: Number of lanes should be an integer >= 1.";
			return false;
		}
		if(max_ppp < 1 || max_ppp > 6) {
			errorMessage = "Invalid input: Max patrons per party should be an integer in range [1,6].";
			return false;
		}
		return true;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(abort)) {
			win.hide();
			System.exit(0);
		}

		if (e.getSource().equals(finished)) {
			lanes = lanesField.getText();
			partysize = partysizeField.getText();
			if (check() == false) {
				error = true;
				win.getContentPane().removeAll();
				addPanels();
			}
			else {
				done = true;
				win.hide();
			}
		}

	}

	public int getNumLanes() {
		while(!done) {
			try {
				Thread.sleep(500);
			} catch (Exception e) {}
		}
		return Integer.parseInt(lanes);
	}

	public int getMaxPPP() {
		return Integer.parseInt(partysize);
	}

	public String getDifficulty() {
		return (String)difficultyCombo.getSelectedItem();
	}
}
