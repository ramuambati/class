
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

class Pair { 
    Integer x; 
    String y; 
  
    // Constructor 
public Pair(Integer x, String y) 
    { 
        this.x = x; 
        this.y = y; 
    } 
} 

public class QueryScore {
	
	
	
	private static String SCOREHISTORY_DAT = "SCOREHISTORY.DAT";
	public Vector loadData() throws IOException, FileNotFoundException{
		Vector scores = new Vector();

		BufferedReader in =
			new BufferedReader(new FileReader(SCOREHISTORY_DAT));
		String data;
		while ((data = in.readLine()) != null) {
			// File format is nick\tfname\te-mail
			String[] scoredata = data.split("\t");
			Vector temp = new Vector();
			temp.add(scoredata[2]);temp.add(scoredata[0]);
			scores.add(temp);
		}
		return scores;
	}
	
	public Vector getMax() throws FileNotFoundException, IOException{
		Vector scores = loadData();
		int mx = -1;
		String name = "";
		for(int i=0;i<scores.size();i++){
			int score = Integer.parseInt((String) ((Vector) scores.get(i)).get(0));
			
			if(score > mx){
				mx = score;name = (String) ((Vector) scores.get(i)).get(1);
			}
		}
		Vector ret = new Vector();
		ret.add(name); ret.add(new Integer(mx));
		return ret;
	}
	
	public Vector getMin() throws FileNotFoundException, IOException{
		Vector scores = loadData();
		int mx = 1000;
		String name = "";
		for(int i=0;i<scores.size();i++){
			int score = Integer.parseInt((String) ((Vector) scores.get(i)).get(0));
			
			if(score < mx && score > 80){
				mx = score;name = (String) ((Vector) scores.get(i)).get(1);
			}
		}
		Vector ret = new Vector();
		ret.add(name); ret.add(new Integer(mx));
		return ret;
	}
	
	private Vector<Pair> sortHashMapByValues(
	        HashMap<String, Integer> passedMap) {
	    List<String> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Integer> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.sort(mapKeys);

	    Vector<Pair> sortedMap =
	        new Vector<Pair>();

	    Iterator<Integer> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Integer val = valueIt.next();
	        Iterator<String> keyIt = ((ArrayList) mapKeys).iterator();

	        while (keyIt.hasNext()) {
	            String key = keyIt.next();
	            Integer comp1 = passedMap.get(key);
	            Integer comp2 = val;

	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.add(new Pair(val, key));
	                break;
	            }
	        }
	    }
//	    Collections.reverse(sortedMap);
	    return sortedMap;
	}
	
	private Vector<Pair> sortByValues(Vector p){
		Vector<Pair> ret = new Vector<Pair>();
		while(p.size() != 0){
			int mx = 1000;
			int ind = -1;
			for(int i=0;i<p.size();i++){
				int var = Integer.parseInt((String) ((Vector) p.get(i)).get(0));
				if(mx > var && var > 85){
					mx = var;
					ind = i;
				}
			}
			if(ind == -1) break;
			Integer x = Integer.parseInt((String) ((Vector) p.get(ind)).get(0));
			String y = (String) ((Vector) p.get(ind)).get(1);
			ret.add(new Pair(x,y));
//			System.out.println(x + " " + y + " " + ind);
			p.remove(ind);
		}
		return ret;
	}
	
	public Vector getTopPlayer(int start, int end, int reverse) throws FileNotFoundException, IOException{
		Vector scores = loadData();
		Vector<Pair> ret = new Vector<Pair>();
		HashMap hm = new HashMap(), sorted = new HashMap<>();
		for(int i=0;i<scores.size();i++){
			int score = Integer.parseInt((String) ((Vector) scores.get(i)).get(0));
			String name = (String) ((Vector) scores.get(i)).get(1);
			if(hm.containsKey(name)){
				int x = (int) hm.get(name);
				hm.replace(name, x + score);
			}else{
				hm.put(name, score);
			}
		}
		ret = sortByValues(scores);
		int sz = ret.size();
		if(reverse != 0) Collections.reverse(ret);
		if(reverse == 2) {
			ret = sortHashMapByValues(hm);
			Collections.reverse(ret);
			return ret;
		}
		int last;
		if(end <= sz){
			last = end;
			// return (Vector) ret.subList(start-1, end);
		}
		else if(end > sz && start <= sz) last = sz;
		else return new Vector<Pair>();
		Vector<Pair> temp = new Vector<Pair>();
		for(int i=start-1;i<last;i++){
			temp.add(ret.get(i));
		}return temp;
	}
}
