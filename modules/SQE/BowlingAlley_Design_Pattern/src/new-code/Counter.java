import java.util.*;
import java.lang.Boolean;

public class Counter {

	private Vector subscribers;
	private int curr_count;
	private int max_count;

	public Counter() {
		subscribers = new Vector();
		curr_count = 0;
		max_count = 10;
	}

	public void increment() {
		curr_count += 1;
		if(curr_count>max_count) {
			curr_count = 0;
		}
		send();
	}

	public int getCount() {
		return curr_count;
	}

	public void setMax(int x) {
		curr_count = 0;
		max_count = x;
	}

	private void send() {	// send events when our state is changd
		for (int i=0; i < subscribers.size(); i++) {
			((CounterObserver)subscribers.get(i)).receiveCounterEvent(curr_count);
		}
	}

	public void subscribe(CounterObserver subscriber) {
		subscribers.add(subscriber);
	}

};
