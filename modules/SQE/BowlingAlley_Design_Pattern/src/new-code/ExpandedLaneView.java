import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ExpandedLaneView implements ActionListener, LaneObserver, CounterObserver, PinsetterObserver {

	private JFrame frame;
	JButton maintenance;
	Container cpanel;
	private PinSetterView psv;
	private LaneView lv;
	private Lane lane;
	private JPanel buttonPanel;
	private int counter_num;
	private int hGap = 5;
	private int vGap = 10;
	private boolean extendedCheck;

	public ExpandedLaneView(final Lane lane, int laneNum) {
		this.lane = lane;
		counter_num = 0;
		extendedCheck = false; 
		psv = new PinSetterView( laneNum );
		Pinsetter ps = lane.getPinsetter();
		ps.subscribe(this);

		lv = new LaneView( lane, laneNum );
		lane.subscribe(this);
		lane.getCounter().subscribe(this);

		frame = new JFrame("Lane " + laneNum + ":");
		cpanel = frame.getContentPane();
		cpanel.setLayout(new GridBagLayout());

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				frame.hide();
			}
		});

		frame.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				// System.out.println("pressed");
				lane.receiveSpacePressed();
			}
			public void keyReleased(KeyEvent e) {
				// System.out.println("released");
			}
			public void keyTyped(KeyEvent e) {
				// System.out.println("typed");
			}
		});

		frame.setFocusable(true);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		Insets buttonMargin = new Insets(4, 4, 4, 4);
		maintenance = new JButton("Maintenance Call");
		maintenance.setBackground( Color.RED );
		JPanel maintenancePanel = new JPanel();
		maintenancePanel.setLayout(new FlowLayout());
		maintenance.addActionListener(this);
		maintenancePanel.add(maintenance);
		buttonPanel.add(maintenancePanel);

		updateFrame();
	}

	private void updateFrame() {

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.NONE;	
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JPanel psv_panel1 = psv.showPins();
		cpanel.add(psv_panel1, gbc);

		JPanel counter_panel = new JPanel();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JLabel numL = new JLabel ("<html><b>Please press the space key to stop the counter: </b><br/><center>" + Integer.toString(counter_num) + "</center></html>" );
		numL.setFont(new Font("Arial", 0, 20));
		counter_panel.add(numL);
		cpanel.add(counter_panel, gbc);


		JPanel psv_heading_panel = new JPanel();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JLabel temp = new JLabel ("PinSetter View" );
		temp.setFont(new Font("Arial", Font.BOLD, 20));
		psv_heading_panel.add(temp);
		cpanel.add(psv_heading_panel, gbc);



		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JPanel psv_panel2 = psv.showTop();
		cpanel.add(psv_panel2, gbc);

		JPanel lv_heading_panel = new JPanel();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JLabel lv_label = new JLabel ("Lane View" );
		lv_label.setFont(new Font("Arial", Font.BOLD, 20));
		lv_heading_panel.add(lv_label);
		cpanel.add(lv_heading_panel, gbc);


		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		JPanel lv_panel = lv.showLane();
		cpanel.add(lv_panel, gbc);

		gbc.fill = GridBagConstraints.NONE;
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(hGap, vGap, hGap, vGap);
		cpanel.add(buttonPanel, gbc);

		frame.pack();
	}

	public void show() {
		frame.show();
	}

	public void hide() {
		frame.hide();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(maintenance)) {
			lane.pauseGame();
		}
	}
	public void receiveLaneEvent(LaneEvent le) {
		if(le.getExtended() != extendedCheck) {
			frame.getContentPane().removeAll();
			frame.repaint();
			extendedCheck = le.getExtended();
		}
		lv.receiveLaneEvent(le);
		updateFrame();
	}

	public void receivePinsetterEvent(PinsetterEvent pe) {
		psv.receivePinsetterEvent(pe);
		updateFrame();
	}

	public void receiveCounterEvent(int x) {
		counter_num = x;
		updateFrame();
	}
}