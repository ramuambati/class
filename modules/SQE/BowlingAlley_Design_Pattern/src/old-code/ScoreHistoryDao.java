import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

public interface ScoreHistoryDao {
	public void addScore(String nick, String date, String score) throws IOException, FileNotFoundException ;
	public Vector getScores(String nick) throws IOException, FileNotFoundException;
}

