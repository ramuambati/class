import java.util.Scanner;


public class Client {
    public static void main (String[] args) {
        System.out.println("Strategy Design pattern - travelling app ");
        System.out.println("Lets decide the strategy to travel ");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your choice - 1 for Driver and 2 for Car");
        int choice = scanner.nextInt();

        Context context = new Context();
        RentingStrategy rentingStrategy = context.getTravelStrategy(choice);
        System.out.println("Using Strategy : "+ rentingStrategy.getClass().getSimpleName());
        rentingStrategy.travelPlan();

    }
}