class Chauffeur implements RentingStrategy {

    // Different algorithm or steps are defined in each sub class
    @Override
    public void travelPlan() {
        System.out.println("You chose to hire a Chauffeur");
        System.out.println("Here are the driver details");
        System.out.println("You driver name is Mr A Smith");
        System.out.println("Contact Number: 7007009009 ");
        System.out.println("Thank you for contacting Avis Agency");
        System.out.println("Have a pleasant trip");
    }
}

