class CarStrategy implements RentingStrategy {

    // Different algorithm or steps are defined in each sub class
    @Override
    public void travelPlan() {
        System.out.println("You chose to rent Car");
        System.out.println("Here are your car details");
        System.out.println("Avis Rental Agency at Gachibowli");
        System.out.println("Midsize Ford Focus");
        System.out.println("TL AM 9867");
        System.out.println("Have a pleasant trip");
    }
}