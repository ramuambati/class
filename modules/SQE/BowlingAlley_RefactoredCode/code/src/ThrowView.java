import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ThrowView implements ActionListener {

    JButton b1, b2;
    JFrame f;
    public int wait;

    public ThrowView() {
        wait = 1;
        f = new JFrame("Panel Example");
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        b1 = new JButton("Throw like a pro");
        // b1.setBounds(50,100,80,30);
        b1.setBackground(Color.green);
        b1.addActionListener(this);

        b2 = new JButton("Kid around with a dummy");
        // b2.setBounds(100,100,80,30);
        b2.setBackground(Color.red);
        b2.addActionListener(this);

        buttonPanel.add(b1);
        buttonPanel.add(b2);

        f.getContentPane().add(buttonPanel, "Center");
        f.pack();
    }

    public void show(String name) {
        f.setTitle(name + "'s turn");
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(b1)) {
            wait = 1;
        } else if (e.getSource().equals(b2)) {
            wait = 2;
        }
    }
}