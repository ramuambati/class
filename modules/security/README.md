# Network Security

## Setup Instructions

- Open up the command prompt
- Do a `git pull`in your local`2019-sum-sesc/class` repository. Alternatively, if you don't remember where you had cloned it earlier, clone it again by running `git clone https://gitlab.com/vxcg/teach/2019-sum-sesc/class`
- Find `Alice.py`, `Bob.py` and `BufferOverflow.c` 

## Diffie Hellman Key Exchange

- Run `python Bob.py`on the command prompt/terminal (in case of Ubuntu).
- Open another command prompt/terminal (in case of Ubuntu) and run `python Alice.py`there.
- Both scripts will prompt you for two secret key values, a (for Alice's key) and b (for Bob's key). Give any random integer as key, for example a=4 and b=3.
- Verify that the calculated encryption key is same for both Alice and Bob. (s=18 for a=4 and b=3)

## Buffer Overflow Attack

- Open the Ubuntu virtual machines on your laptops.
- Start the terminal and check if gcc is installed by running `gcc --version`and if not, install it by running `sudo apt install gcc`
- Run `gcc BufferOverflow.c`  and then `./a.out`
- For name <8 characters, everything works as expected. The real password is "test", and if the password entered is "test", access is granted otherwise its rejected.
- For name>=8 characters, the real password is overwritten with the last few letters of the name due to buffer overflow.
- So if an attacker wanted to set the password to "hack", he would enter the name as "abcdefghhack" and give any random password and the real password would be set to "hack". Verify this.