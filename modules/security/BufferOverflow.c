#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 

void authenticate (char* string1, char* string2)
{
    char buffer1[8];
    char buffer2[8];
    strcpy (buffer1,string1);
    strcpy (buffer2,string2);
    if (strcmp(buffer1,buffer2)==0)
        printf("Access allowed!\n");
}

int main()
{
    char password[8];	// The password entered by the user
    char real_passwd[8]="test";	// This is the real password
    char name[8];		// The username

    // retrieve the user information
    printf ("Enter your name: ");
    gets(name);
    printf("Enter your password: ");
    gets(password);
    printf("Your name and password are %s and %s.\n",name,password);
    printf("The real password for %s is %s.\n",name,real_passwd);
    // Authenticate password against real_passwd
    authenticate(password,real_passwd);
    printf("The address of name is: %p\n", name);
    printf("The address of real_passwd is: %p\n", real_passwd);
    printf("The address of password is: %p\n", password);
    return 0;
}
