import socket

p = 23	# p is a publicly agreed prime number known to both Bob and Alice
g = 5	# g is a primitive root modulo p, also known to both Bob and Alice

def calculateMessage():
	while (True):
		# a is an secret integer known only to Alice
		a = input("Enter a random secret for Alice: ")
		try:
			a = int(a)
			break
		except:
			print ("Please enter a valid integer")
			continue

	# Computing the message to be sent to Alice over the channel, ie. g^a mod p
	A = (g ** a) % p
	return (A,a)

def calculateSecret(message, secretKey):
	''' 
	The secret key is computed as message^secretKey mod p, where:
	 - message: B, the number transported from Bob over the channel
	 - secretKey: a, Alice's secret key used in calculation of the message
	 '''
	secret = (message ** secretKey) % p
	return secret

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 12345 # Port over which Alice and Bob communicate

# connect to the server (Bob) on local computer
s.connect(('127.0.0.1', port)) 

(A,a) = calculateMessage()

print ("Message calculated by Alice: ", A)

s.send(str(A).encode("utf-8"))

# receive data from Bob
B = int(s.recv(1024).decode("utf-8"))

print ("Message received from Bob: ", B)

s.close()

secret = calculateSecret(B,a)

print ("The value of the secret s is: ", secret)

print ("Thus, using Diffie Hellman key exchange a secret key is transferred without the eavesdropper\
 being able to calculate the secret key despite knowing the values of A, B, g and p.")