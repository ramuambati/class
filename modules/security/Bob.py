import socket

p = 23	# p is a publicly agreed prime number known to both Bob and Alice
g = 5	# g is a primitive root modulo p, also known to both Bob and Alice

def calculateMessage():	
	while (True):
		# b is an secret integer known only to Bob
		b = input("Enter a random secret for Bob: ")
		try:
			b = int(b)
			break
		except:
			print ("Please enter a valid integer")
			continue

	# Computing the message to be sent to Alice over the channel, ie. g^b mod p
	B = (g ** b) % p
	return (B,b)

def calculateSecret(message, secretKey):
	''' 
	The secret key is computed as message^secretKey mod p, where:
	 - message: A, the number transported from Alice over the channel
	 - secretKey: b, Bob's secret key used in calculation of the message
	 '''
	secret = (message ** secretKey) % p
	return secret


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

port = 12345 # Port over which Alice and Bob communicate

# Next bind to the port 
# we have not typed any ip in the ip field 
# instead we have inputted an empty string 
# this makes the server listen to requests  
# coming from other computers on the network 
s.bind(('', port))

(B,b) = calculateMessage()

print ("Message calculated by Bob: ", B)

# put the socket into listening mode 
s.listen(5)
print ("Bob is listening")

while True:
	# Establish connection with Alice 
	c, addr = s.accept()   
	
	A = int(c.recv(1024).decode("utf-8"))
	print ("Message received from Alice: ", A)
	# send the calculated message to Alice
	c.send(str(B).encode("utf-8"))

	# Close the connection with the client 
	c.close()
	break

s.close()

secret = calculateSecret(A,b)

print ("The value of the secret s is: ", secret)

print ("Thus, using Diffie Hellman key exchange a secret key is transferred without the eavesdropper\
 being able to calculate the secret key despite knowing the values of A, B, g and p.")