## Experiment on fixing bugs and security flaws for python codes given at [src/problems](src/problems)

1. You are given a bunch of programs with bugs that are needed to be fixed.
2. Run the program and observe output to find out what may be going wrong.
3. Edit program accordingly to get desired output.
4. Put comments wherever you feel are necessary.

**Description for each program i.e, what it's supposed to do, its inputs and desired output is given as comments in the program**

