#This program is an example for sequential search

# Input : none
# Output : "Target 3 found at position: 2 in 3 iterations
#           Target 9 not found in the list"

# Logic :
# iterate over the array to find out position of target while counting iterations

def sequentialSearch(target, List):
    '''This function returns the position of the target if found else returns -1'''
    position = 0
    global iterations
    while position < len(List)+1:
        iterations += 1
        if target == List[position]:
            return position
        position += 1
    return -1

if __name__ == '__main__':
    List = [1, 2, 3, 4, 5, 6, 7, 8]
    target = 3
    ans = sequentialSearch(target, List)
    if ans == -1:
        print('Target',target,'found at position:',ans,'in',iterations,'iterations')
    else:
        print('Target',target,'not found in the list')

    target = 9
    ans = sequentialSearch(target, List)
    if ans == -1:
        print('Target',target,'found at position:',ans,'in',iterations,'iterations')
    else:
        print('Target',target,'not found in the list')
