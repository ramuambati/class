# This example shows how to use Python with JSON

# Input : none
# Output : json data from example.json
#          data['menu']['value'] list
#          updated json data from example.json
#          updated data['menu']['value'] list

import json

def storeJSON(fileName, data = {}):
    fd = open(fileName, 'w')
    json.dump(data, fd, indent = 4, separators = (',', ': '))

def loadJSON(fileName):
    fd = open(fileName)
    data = json.load(fd)
    print(data)
    return data

if __name__ == '__main__':
    data = loadJSON('example.json')
    print(data['menu']['value'])
    data['menu']['value'] = 'movie'
    storeJSON('example.json', data)
    loadJSON('example.json')
    print(data['menu']['value'])
