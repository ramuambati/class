#This program shows an example of bubble sort

# Input : none
# Output : sorted list

# Logic :
# i{0, n-1} => i iterates over 0 to n-1
# j{0, i} => j iterates over 0 to n-1
# swap(j) => swap if jth term is greater than j+1th
# after every j loop we end up with max at end
# thus A has i sorted elements in the end

def bubbleSort(List):
    for i in range(len(List)):
        for j in range(len(List) - 1, j, -1):
            if List[j] < List[j - 1]:
                List[j], List[j - 1] = List[j - 1], List[j]
    return List

if __name__ == '__main__':
    List = [3, 4, 2, 6, 5, 7, 1, 9]
    print('Sorted List:',bubbleSort(List))
