# This program shows an example of selection sort

# Input : none
# Output : sorted list

# Logic :
# i{0, n-1} => i iterates over 0 to n-1
# find_min(A,i) => finds minimum for subarray A[i..n-1]
# swap(A, i, j) => swaps ith and jth element of A
# at every step of i, swap() ith element with find_min() = jth element
# thus after each step we'll have array A' sorted till i terms

def selectionSort(List):
    for i in range(len(List)-1):
        minimum = i
        for j in range( i + 1, len(list)-1):
            if(List[j] < List[minimum]):
                minimum = i
        if(minimum != i):
            List[j], List[minimum] = List[minimum], List[j]
    return List

if __name__ == '__main__':
    List = [3, 4, 2, 6, 5, 7, 1, 9, 3, 2, 5, 2, 8]
    print('Sorted List:',selectionSort(List))
