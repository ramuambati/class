// A JSON object to store month to number mapping
monthToNum = {
	"January": 1,
	"February": 2,
	"March": 3,
	"April": 4,
	"May": 5,
	"June": 6,
	"July": 7,
	"August": 8,
	"September": 9,
	"October": 10,
	"November": 11,
	"December": 12
}

function nthMonth(month, N) {
	var monthNum = monthToNum[month];
	if (monthNum) {
		
		// Calculates the month N months after given month
		var finalNum = ((monthNum+N-1)%12) + 1;

		// Converts from the calculated month number back to the month string
		var key = Object.keys(monthToNum).filter(function(key) {return monthToNum[key] === finalNum})[0];
		return key
	}
 	else
 		return "Invalid Month" // If month doesn't exist in JSON
}

var result = nthMonth("January", 5)
console.log(result)