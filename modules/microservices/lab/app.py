from flask import Flask, jsonify, request #import objects from the Flask model
app = Flask(__name__) #define app using Flask

# Creation of APIs for Task 1
# Returns a JSON with two fields, username and amount

@app.route('/account/savings', methods=['GET'])
def getSavings():
	return jsonify({'name' : 'admin', 'amount': 4000})

@app.route('/account/deposit', methods=['GET'])
def getDeposit():
	return jsonify({'name' : 'admin', 'amount': 10000})

@app.route('/account/ppf', methods=['GET'])
def getPPF():
	return jsonify({'name' : 'admin', 'amount': 1000})


@app.route('/account/demat', methods=['GET'])
def getDemat():
	return jsonify({'name' : 'admin', 'amount': 50000})

# Creation of APIs for Task 2
# Given a number rating, returns a string descriptor of the feedback

@app.route('/ratings', methods=['POST'])
def ratingsMapper():
	rating = request.form['rating']
	ratingToMsgDict = {
		"1": "Very bad",
		"2": "Bad",
		"3": "Average",
		"4": "Good",
		"5": "Very good"
	}
	return jsonify({'message': ratingToMsgDict[rating]})


# Creation of APIs for Task 3
# Returns a JSON with two fields, method of transfer and message for completion of transfer

@app.route('/transfer/imps', methods=['POST'])
def impsTransfer():
	amount = request.form['amount']
	method = "IMPS"
	message = "Instant Transfer!"
	return jsonify({'method' : method, 'message': message})

@app.route('/transfer/rtgs', methods=['POST'])
def ppfTransfer():
	amount = request.form['amount']
	method = "RTGS"
	message = "Transaction will be completed in "
	return jsonify({'method' : method, 'message': message})

@app.route('/transfer/neft', methods=['POST'])
def neftTransfer():
	print (request.form)
	amount = request.form['amount']
	method = "NEFT"
	message = "Transaction will be completed in "
	return jsonify({'method' : method, 'message': message})

@app.route('/transfer/intrabank', methods=['POST'])
def intrabankTransfer():
	amount = request.form['amount']
	method = "Intrabank"
	message = "Instant Tranfer!"
	return jsonify({'method' : method, 'message': message})

if __name__ == '__main__':
	app.run(debug=True, port=8080) #run app on port 8080 in debug mode