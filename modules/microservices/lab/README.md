# Lab 3

## Lab Instructions

- There are three html files, Q1.html, Q2.html and Q3.html
- Your job, in each task, is to build a microservice that meets the requirements specified on these HTML pages
- Boilerplate code has been made available to help you get started
- On clicking the submit button, the microservice should fetch the required details from the APIs and return them in the specified format in the div with id "status".