var currentRating = 0;

$(function () {
  // IGNORE THIS: NO CHANGES NEEDED HERE

  /* A function for creating the rating div, that changes the
   highlighted stars on hover and selects a star on clicking */

	 $(".starRating,.star").each( function(){
        for (var i = 0; i < 5; i++)
           $("<span>").appendTo(this);
           
        $("span", this).each(function(index) {
          $(this).click(
          	function() { currentRating = index+1;
            						$(this).prevAll().add(this).addClass("chosen") }).click(
            function() { $(this).nextAll().removeClass("chosen") } 
          );
        	$(this).hover(
              function() {$(this).prevAll().add(this).addClass("hoverChosen") },
              function() { $(this).prevAll().add(this).removeClass("hoverChosen") }  
          );
 			  });   
   });
});

function microservice_rating() {
  // The primary microservice function: Your code goes here

  /* Your task is to make an AJAX POST request to the /ratings API
   and set the contents of the div with id status to the response,
   as done in the first task */
}