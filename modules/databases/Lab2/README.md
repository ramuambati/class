## Questions

1. Write a query to find all the movies named Sabrina and return their imdb ratings.
2. Return the name of the actor who has acted in the movie Forrest Gump.
3. Find actors who have been in less than 3 movies
4. Find all the movies which have been given a rating of over 3.0 by user “Katie Brown”
5. In the above question, return the title and imdb Ratings for movies which have “Al Pacino” acting in them
