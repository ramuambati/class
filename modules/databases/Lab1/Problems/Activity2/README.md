# Lab Activity 2: MongoDB

## Installation Instructions

- [Download](https://www.mongodb.com/download-center/community?jmp=docs) mogodb installer. Make sure the version is 4.0.10, OS is Windows 64-bit x64 and package is msi

- Choose complete Installation, once done you can run server by mongod.exe and client by mongo.exe
    
- Do a `git pull`to download database_dump from this git repo 
  
- cd to bin folder in mogodb, place database-dump folder inside bin folder

- Open a powershell tab and run `.\mongorestore.exe -d Lab2 database_dump`

- Now run `.\mongod.exe`

- Open new powershell tab and run `.\mongo.exe`, type `use Lab2` to start using our database

- Do `show tables` to make sure database is loaded 

## Exercises

Write mongoDB queries to fetch the appropriate results specified:

1. Display rollnos and emails of all students.  
2. Display rollnos and emails of students born between 1st Aug 1990  and  21st Jan 1993.  
3. Display rollnos and emails of all students sorted by their names.  
4. Display rollnos of students who study "maths".  
5. Display rollnos of students who study some subject under "Lily".  
6. Display average marks of students for each subject.  
7. Display average marks of students for each subject which has atleast 10 students studying it.

## Schema

students (rollno, name, email, bday)
courses (id, name)
studies (rollno, courseid, marks)
faculty (id, name, dept)
teaches (facultyid, courseid)
