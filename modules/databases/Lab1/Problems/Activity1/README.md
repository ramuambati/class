# Lab Activity 1: SQLite

## Installation Instructions

- Download the sqlite3 zip file from here: [https://www.sqlite.org/2019/sqlite-tools-win32-x86-3280000.zip](https://www.sqlite.org/2019/sqlite-tools-win32-x86-3280000.zip)

-   Extract the zip folder just downloaded.
    
- Do a `git pull`to download database.db from this git repo 
  
- Open the command prompt and navigate to the folder location to where the zip file was extracted. 

- Now run `.\sqlite3 database.db`

## Exercises

Write SQLite queries to fetch the appropriate results specified:

1. Display rollnos and emails of all students.  
2. Display rollnos and emails of students born between 1st Aug 1990  and  21st Jan 1993.  
3. Display rollnos and emails of all students sorted by their names.  
4. Display rollnos of students who study "maths".  
5. Display rollnos of students who study some subject under "Lily".  
6. Display average marks of students for each subject.  
7. Display average marks of students for each subject which has atleast 10 students studying it.