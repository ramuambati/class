1. select email, rollno from students;

2. select email, rollno from students where strftime("%Y%m%d",bday) >= strftime("%Y%m%d","1990-08-01") and strftime("%Y%m%d",bday) <= strftime("%Y%m%d","1993-01-21");

3. select email, rollno from students order by name;

4. select rollno from courses inner join studies on studies.courseid = courses.id where courses.name = "maths";

5. select students.rollno from students, studies, faculty, teaches where students.rollno == studies.rollno and studies.courseid == teaches.courseid and teaches.facultyid == faculty.id and faculty.name == "Lily";

6. select name, avg(marks) from courses inner join studies on studies.courseid = courses.id group by studies.courseid;

7. select name, avg(marks) from courses inner join studies on studies.courseid = courses.id group by studies.courseid having count(*)>=10;
