// To switch to Lab 2 database, run `use Lab2`

// To increase batch size limit from 20, run DBQuery.shellBatchSize = 300

1. db.students.find({},{"rollno":1,"name":1,_id:0})

2. db.students.find(
	{"$and": [
		{"bday":{"$gte": "1990-08-01"}},
		{"bday":{"$lte": "1993-01-21"}}]},
	{"email":1,"rollno":1,_id:0})

3. db.students.find({},{"rollno":1,"name":1,_id:0}).sort( { name: 1 })

4. db.courses.aggregate([
	{ $match: {'name': {"$eq": "maths"}} },
    {
        $lookup:{
            from: "studies",
            localField : "id",
            foreignField : "courseid",
            as : "joinedCourseStudent"
        }
    },
    { $unwind: "$joinedCourseStudent" },
  	{ 
  		$project: {
    		"rollno":"$joinedCourseStudent.rollno",
    		"_id":0
  		}
   	}
])

5. db.teaches.aggregate([
	// Join with studies table
    {
        $lookup:{
            from: "studies",   // other table name
            localField : "courseid",   // name of teaches table field
            foreignField : "courseid",  // name of studies table field
            as : "joinedCourseStudent"  // alias for studies table
        }
    },
    { $unwind: "$joinedCourseStudent" },   // $unwind used for getting data in object or for one record only

    {
        $lookup:{
            from: "faculty",
            localField : "facultyid",
            foreignField : "id",
            as : "joinedCourseFaculty"
        }
    },
    { $unwind: "$joinedCourseFaculty" },
    
    // define some conditions here
    {
        $match:{
            'joinedCourseFaculty.name': {"$eq": "Lily"}
        }
    },

    // define which fields are you want to fetch
  	{ 
  		$project: {
    		"rollno":"$joinedCourseStudent.rollno",
    		"_id":0
  		}
   	}
])

6. db.courses.aggregate([
    {
        $lookup:{
            from: "studies",
            localField : "id",
            foreignField : "courseid",
            as : "joinedCourseStudent"
        }
    },
    { $unwind: "$joinedCourseStudent" },
   	{
   		$group: {_id: "$name", pop: {$avg:"$joinedCourseStudent.marks"}} 
   	},
   	{ 
  		$project: {
    		"name":"$_id",
    		"marks": "$pop",
    		"_id": 0
  		}
   	} 		
])

7. db.courses.aggregate([
    {
        $lookup:{
            from: "studies",
            localField : "id",
            foreignField : "courseid",
            as : "joinedCourseStudent"
        }
    },
    { $unwind: "$joinedCourseStudent" },
   	{
   		$group: {_id: "$name", pop: {$avg:"$joinedCourseStudent.marks"},
               count: { $sum: 1 }},  
   	},
   	{ $match: {'count': {"$gt": 10}} },
   	{ 
  		$project: {
    		"name":"$_id",
    		"marks": "$pop",
    		"_id": 0
  		}
   	}

   		
])