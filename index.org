#+title:  IIIT Summer 2019: Trends in S/W Engg
* Location
 Seminar Hall 1 (SH1), IIIT Hyderabad

* Instructors
 - Venkatesh Choppella, SERC, IIIT (vxc)
 - Raghu  Reddy, SERC, IIIT  
 - Suresh Purini, CSG, IIIT 
 - Vikram Pudi, CDS, IIIT 
 - Ramesh Loganathan, SERC, IIIT

* Course infrastructure

|---------------------+---+-------------------------------+---------------+---|
| Slack Channels:     |   |                               |               |   |
| To be Announced     |   | class workspace               | by invitation |   |
| [[https://2019-sum-sesc-instr.slack.com/home][2019-sum-sesc-instr]] |   | instructors workspace         | by invitation |   |
|---------------------+---+-------------------------------+---------------+---|
| [[https://gitlab.com/vxcg/teach/2019-sum-sesc/class][Gitlab class repo]]   |   | All class lectures and        |               |   |
|                     |   | lab info will be posted here. |               |   |
|---------------------+---+-------------------------------+---------------+---|



* Modules

|-----+-----------------------+---------------------|
| No. | Module                | Instructor          |
|-----+-----------------------+---------------------|
|  1. | [[./modules/programming-paradigms/index.org][Programming Paradigms]] | Venkatesh Choppella |
|-----+-----------------------+---------------------|



